var config = require('config');
var express = require('express');
var app = express();

var index = require('./routes/index');
var users = require('./routes/users');

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.set('view engine', 'jade');

app.get('/', index);

app.post('/send_mail', users);

app.listen(config.get('PORT'));
console.log("Express server started on port %d", config.get('PORT'));