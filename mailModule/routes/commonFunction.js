var express = require('express');
var sendResponse = require('./sendResponse');
var config = require('config');
exports.checkBlank = function(arr) {
    var arrlength = arr.length;
    for (var i = 0; i < arrlength; i++) {
        if (arr[i] === '' || arr[i] === undefined || arr[i] === '(null)') {
            return 1;
            break;
        }
    }
    return 0;
};


exports.sendEmailToUser = function(receiverMailId, message, subject) {
    var nodemailer = require("nodemailer");
    var smtpTransport = nodemailer.createTransport("SMTP", {
        service: "Gmail",
        auth: {
            user: config.get('emailSettings').email,
            pass: config.get('emailSettings').password

        }
    });

// setup e-mail data with unicode symbols
    var mailOptions = {
        from: config.get('emailSettings').email,
        to: receiverMailId,
        subject: subject,
        text: message

    };

// send mail with defined transport object
    smtpTransport.sendMail(mailOptions, function(error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Message sent');

        }
    });
};