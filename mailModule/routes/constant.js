/**
 * The node-module to hold the constants for the server
 */


function define(obj, name, value) {
    Object.defineProperty(obj, name, {
        value:        value,
        enumerable:   true,
        writable:     false,
        configurable: false
    });
}


exports.responseStatus = {};

define(exports.responseStatus, "PARAMETER_MISSING", 100);

exports.responseMessage = {};
define(exports.responseMessage, "PARAMETER_MISSING", "Some Parameters Missing");






