var sendResponse = require('./sendResponse');
var func = require('./commonFunction');
var express = require('express');
var router = express.Router();

router.post('/send_mail', function(req, res) {
    var emailTo = req.body.email;
    var subName = req.body.sub_name;
    var message = req.body.message;
    var manValues = [emailTo, message];
    console.log(emailTo);
    console.log(message);
    console.log(subName);
    var flag = func.checkBlank(manValues);
    if (flag) {
        sendResponse.parameterMissingError(res);
    }
    else {
        func.sendEmailToUser(emailTo, subName, message);
    }
});

module.exports = router;

